package util;

import java.io.File;
import java.io.IOException;

public class Repository {

	/*
	 * static { try { Runtime.getRuntime().exec("attrib +H InputRepository");
	 * Runtime.getRuntime().exec("attrib +H OutputRepository"); } catch
	 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace();
	 * } }
	 */
	public static boolean createDepartmentDirectory(String id) {
		String parentI = "InputRepository/";
		String parentO = "OutputRepository/";
		boolean createdI = false;
		boolean createdO = false;
		File fi = new File(parentI + id);
		File fo = new File(parentO + id);
		if (!fi.exists() && !(fo.exists())) {
			createdI = fi.mkdir();
			createdO = fo.mkdir();
		}
		if (createdI && createdO) {
			try {
				Runtime.getRuntime().exec("attrib +H " + parentI + id);
				Runtime.getRuntime().exec("attrib +H " + parentO + id);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return createdI && createdO;

	}

	public static boolean createUserDirectory(String dptId, String uId) {
		String parentI = "InputRepository/";
		String parentO = "OutputRepository/";
		boolean createdI = false;
		boolean createdO = false;
		File fi = new File(parentI + uId);
		File fo = new File(parentO + uId);
		if (!fi.exists() && !(fo.exists())) {
			createdI = fi.mkdir();
			createdO = fo.mkdir();
		}
		if (createdI && createdO) {
			try {
				Runtime.getRuntime().exec("attrib +H " + parentI + uId);
				Runtime.getRuntime().exec("attrib +H " + parentO + uId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return createdI && createdO;

	}

}
