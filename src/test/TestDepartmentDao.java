package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import bean.DepartmentBean;
import dao.departmentDao;
import services.departmentValid;

public class TestDepartmentDao {
	DepartmentBean db = new DepartmentBean();
	departmentDao dao = new departmentDao();
	departmentValid dv = new departmentValid();

	@Test
	public void testCreateDepartment() {

		db.setDepartmentName("mechasnical");
		db.setDepartmentKey("hahaha");
		String actual = dv.createDepartment(db);
		assertEquals("ab118", actual);

		/*
		 * db.setDepartmentId("co10155");
		 * db.setDepartmentName("computer science");
		 * db.setDepartmentKey("abcd"); String actual = dv.createDepartment(db);
		 * assertEquals("INVALID", actual);
		 * 
		 * db.setDepartmentId("co102"); db.setDepartmentName(null);
		 * db.setDepartmentKey("abcd"); String actual = dv.createDepartment(db);
		 * assertEquals("INVALID", actual);
		 * 
		 * db.setDepartmentId("co10155");
		 * db.setDepartmentName("computer science"); db.setDepartmentKey(null);
		 * String actual = dv.createDepartment(db); assertEquals("INVALID",
		 * actual);
		 * 
		 * db.setDepartmentId("co105");
		 * db.setDepartmentName("computer science"); db.setDepartmentKey("");
		 * String actual = dv.createDepartment(db); assertEquals("INVALID",
		 * actual);
		 * 
		 * db.setDepartmentId("co106");
		 * db.setDepartmentName("computer science"); db.setDepartmentKey(null);
		 * String actual = dv.createDepartment(db); assertEquals("INVALID",
		 * actual);
		 */

	}

	@Test
	public void testDeleteDepartment() {
		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("co102");
		 * al.add("co103"); int actual = dv.deleteDepartment(al);
		 * assertEquals(2, actual);
		 */

		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("co10245");
		 * al.add("co104"); int actual = dv.deleteDepartment(al);
		 * assertEquals(1, actual);
		 */
		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("co10244");
		 * int actual = dv.deleteDepartment(al); assertEquals(0, actual);
		 */

		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add(null); int
		 * actual = dv.deleteDepartment(al); assertEquals(0, actual);
		 */
	}

	@Test
	public void testViewDepartment() {
		/*
		 * assertNotNull(dv.viewDepartment("co101"));
		 * assertNull(dv.viewDepartment("324342"));
		 * assertNull(dv.viewDepartment(null));
		 */
	}

	@Test
	public void testUpdateDepartmentName() {
		/*
		 * db.setDepartmentId("ab106"); assertTrue(dv.updateDepartmentName(db,
		 * "admin"));
		 */

		/*
		 * db.setDepartmentId("abdf106");
		 * assertFalse(dv.updateDepartmentName(db, "admin"));
		 */

		/*
		 * db.setDepartmentId("abdf106");
		 * assertFalse(dv.updateDepartmentName(db, "admin"));
		 */

	}

	@Test
	public void testUpdateDepartmentKey() {
		/*
		 * db.setDepartmentId("co101"); assertTrue(dv.updateDepartmentKey(db,
		 * "11223344"));
		 */

		/*
		 * db.setDepartmentId("co01"); assertFalse(dv.updateDepartmentKey(db,
		 * "11223344"));
		 */

		/*
		 * db.setDepartmentId("co101"); assertFalse(dv.updateDepartmentKey(db,
		 * ""));
		 */
	}

}
