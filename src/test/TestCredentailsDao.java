package test;

import static org.junit.Assert.*;

import org.junit.Test;

import bean.CredentialBean;
import bean.ProfileBean;
import services.CredentialValid;

public class TestCredentailsDao {
	CredentialBean cb = new CredentialBean();
	CredentialValid cv = new CredentialValid();

	@Test
	public void testRegisterUser() {
		ProfileBean pb = new ProfileBean();
		pb.setFirstName("arjun");
		pb.setLastName("goel");
		pb.setEmailId("arjun.goel@gmail.com");
		pb.setDepartmentId("el108");
		pb.setDateOfBirth("01-13-1994");
		pb.setContactNumber("9084543450");
		pb.setAddress("haldighati , bharat");
		pb.setGender("male");
		cb.setPassword("kuchbhi");
		cb.setUserType("u");

		String actual = cv.registerUser(cb, pb);
		assertEquals("arj@1011", actual);

		/*
		 * ProfileBean pb = new ProfileBean(); pb.setFirstName("rohit");
		 * pb.setLastName("saini"); pb.setEmailId("amit.saini@gmail.com");
		 * pb.setDepartmentId("co101"); pb.setDateOfBirth("23-11-1993");
		 * pb.setContactNumber("9080706050");
		 * pb.setAddress("raja ki kothi , hastinapur");
		 * 
		 * cb.setPassword("abcd1234"); cb.setUserType("u");
		 * 
		 * String actual = cv.registerUser(cb, pb); assertEquals("roh@1006",
		 * actual);
		 */

		/*
		 * ProfileBean pb = new ProfileBean(); pb.setFirstName(null);
		 * pb.setLastName("saini"); pb.setEmailId("amit.saini@gmail.com");
		 * pb.setDepartmentId("co101"); pb.setDateOfBirth("23-11-1993");
		 * pb.setContactNumber("9080706050");
		 * pb.setAddress("raja ki kothi , hastinapur");
		 * 
		 * cb.setPassword("abcd1234"); cb.setUserType("u");
		 * 
		 * String actual = cv.registerUser(cb, pb); assertEquals("INVALID",
		 * actual);
		 */

		/*
		 * ProfileBean pb = new ProfileBean(); pb.setFirstName("amit");
		 * pb.setLastName("saini"); pb.setEmailId("amit.saini@"); /// REGEX FOR
		 * EMAIL NOT WORRKING pb.setDepartmentId("co101");
		 * pb.setDateOfBirth("23-11-1993"); pb.setContactNumber("9080706050");
		 * pb.setAddress("raja ki kothi , hastinapur");
		 * 
		 * cb.setPassword("abcd1234"); cb.setUserType("u");
		 * 
		 * String actual = cv.registerUser(cb, pb); assertEquals("INVALID",
		 * actual);
		 */

		/*
		 * ProfileBean pb = new ProfileBean(); pb.setFirstName("varun");
		 * pb.setLastName("saini"); pb.setEmailId("amit.saini@gmail.com");
		 * pb.setDepartmentId("co11"); pb.setDateOfBirth("23-11-1993");
		 * pb.setContactNumber("9080706050");
		 * pb.setAddress("raja ki kothi , hastinapur");
		 * 
		 * cb.setPassword("abcd1234"); cb.setUserType("u");
		 * 
		 * String actual = cv.registerUser(cb, pb); assertEquals("FAIL",
		 * actual);
		 */

	}

	@Test
	public void deleteCredentials() {
		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("1002");
		 * al.add("1001"); int actual = cv.deleteCredential(al); assertEquals(0,
		 * actual);
		 */

		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("ami@1001");
		 * al.add("aru@1003"); int actual = cv.deleteCredential(al);
		 * assertEquals(2, actual);
		 */

		/*
		 * ArrayList<String> al = new ArrayList<String>(); al.add("dee@1002");
		 * al.add("raj@1004"); al.add("anu@1010"); int actual =
		 * cv.deleteCredential(al); assertEquals(2, actual);
		 */
	}

	@Test
	public void testChangePassword() {
		/*
		 * cb.setUserId("ran@1005"); cb.setPassword("qwer");
		 * assertTrue(cv.changePasseord(cb, "123456"));
		 */

		/*
		 * cb.setUserId("ran@1005"); cb.setPassword("qwer");
		 * assertFalse(cv.changePasseord(cb, "123456"));
		 */
	}

	@Test
	public void testChangeLoginStatus() {
		// assertTrue(cv.changeLoginStatus("roh@1006", (byte)1));

		// assertFalse(cv.changeLoginStatus("roh@106", (byte)1));

		// assertFalse(cv.changeLoginStatus("roh@106", (byte)4));

	}

	@Test
	public void testViewCredentials() {
		// assertNotNull(cv.viewCredential("roh@1006"));

		// assertNull(cv.viewCredential("roh@16"));

		// assertNull(cv.viewCredential("abc@1633"));
	}
}
