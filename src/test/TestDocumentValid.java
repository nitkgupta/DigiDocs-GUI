package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import bean.DocumentBean;
import services.DocumentValid;

public class TestDocumentValid {
	DocumentValid dv = new DocumentValid();
	DocumentBean db = new DocumentBean();

	@Test
	public void testCreateDocument() {
		
//		 db.setDocumentName("resident");
//		 db.setCreatedByUser("roh@1006");
//		 db.setDepartment("ab106"); 
//		 db.setCategory("voter id");
//		 String actual = dv.createDocument(db); 
//		 assertEquals("re20180323113", actual);
		 

		/*
		 * db.setDocumentName(null); db.setSize(34);
		 * db.setCreatedByUser("tri@1007"); db.setDepartment("ab106");
		 * db.setLastModifiedOn("18-09-2017"); String actual =
		 * dv.createDocument(db); assertEquals("INVALID", actual);
		 */

		/*
		 * db.setDocumentName("aadhar"); db.setSize(34);
		 * db.setCreatedByUser("tri@1007"); db.setDepartment("");
		 * db.setLastModifiedOn("18-09-2017"); String actual =
		 * dv.createDocument(db); assertEquals("FAIL", actual);
		 */
	}

	@Test
	public void testDeleteDocument() {
		
//		 ArrayList<String> al = new ArrayList<String>();
//		 al.add("aa20180215108"); int actual = dv.deleteDocument(al);
//		 assertEquals(1, actual);
		
	}

	@Test
	public void testUpdateDocumentSize() {
//		 assertTrue(dv.updateDocumentSize("re20180215109", 555));

		// assertFalse(dv.updateDocumentSize("re2018021509", 55));

		// assertFalse(dv.updateDocumentSize(null, 55));
	}

	@Test
	public void testUpdateLastModifies() {
//		assertTrue(dv.updateLastModifiedOn("re20180215109"));
//
//		assertTrue(dv.updateLastModifiedOn("aa20180215108"));
//
//		assertFalse(dv.updateLastModifiedOn("re201802159"));
//
//		assertFalse(dv.updateLastModifiedOn(null));

	}

	@Test
	public void testUpdateLastAccessedBy() {
		// assertTrue(dv.updateLastAccessedBy("aa20180215108", "tri@1007"));

		// assertFalse(dv.updateLastAccessedBy("", "tri@1007"));

		// assertFalse(dv.updateLastAccessedBy(null, "tri@1007"));

		// assertTrue(dv.updateLastAccessedBy("re20180215109", "roh@1006"));

	}

	@Test
	public void testUpdateLastAccessedOn() {
		//assertTrue(dv.updateLastAccessedOn("aa20180215108"));

		//assertFalse(dv.updateLastAccessedOn(""));

		// assertFalse(dv.updateLastAccessedOn(null));

		//assertTrue(dv.updateLastAccessedOn("re20180215109"));

	}

	@Test
	public void testViewDocument() {
		// assertNotNull(dv.viewDocument("aa20180215108"));

		// assertNull(dv.viewDocument("zdfgn"));

		// assertNull(dv.viewDocument("aa20180215111"));
	}
	
	@Test
	public void testuploadDocumentImage()
	{
//		assertTrue(dv.uploadDocumentImage("aa20180215108", "D:\\yamaha-27v.jpg"));
//		
//		assertFalse(dv.uploadDocumentImage("", "D:\\yamaha-27v.jpg"));
//		
//		assertFalse(dv.uploadDocumentImage("aa20180215108", ""));
//		
//		assertFalse(dv.uploadDocumentImage("aa20180215108", null));
//		
//		assertFalse(dv.uploadDocumentImage("aa20180215108", "fjas\\dd"));
	}
	
	@Test
	public void testSaveDocumentDoc() throws IOException
	{
//		assertTrue(dv.saveDocumentOutput("aa20180215108", "D:\\final quiz.docx"));
		
//		assertFalse(dv.saveDocumentOutput("", "D:\\final quiz.docx"));
//		
//		assertFalse(dv.saveDocumentOutput("aa20180215108", ""));
		
//		assertFalse(dv.saveDocumentOutput("aa20180215108", null));
//		
//		assertFalse(dv.saveDocumentOutput("aa20180215108", "fdsff\\ff"));
		
		assertTrue(dv.uploadDocumentImage("He20180324114", "F:\\out.jpg"));
		
	}
	
	
}
