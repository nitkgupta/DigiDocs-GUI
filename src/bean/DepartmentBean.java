package bean;

public class DepartmentBean {
	private String departmentId;
	private String departmentName;
	private String DepartmentKey;

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentKey() {
		return DepartmentKey;
	}

	public void setDepartmentKey(String departmentKey) {
		DepartmentKey = departmentKey;
	}

}
