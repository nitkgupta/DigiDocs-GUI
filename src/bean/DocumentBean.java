package bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DocumentBean {
	private String documentId;
	private String documentName;
	private int size;
	private String createdByUser;
	private Date createdOn;
	private Date lastModiedOn;
	private String department;
	private Date lastAccessedOn;
	private String lastAccessedBy;
	private String category;
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String departmentName) {
		this.documentName = departmentName;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getCreatedByUser() {
		return createdByUser;
	}

	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	// following method not required as cretedOn date is set automatically.
	public void setCreatedOn(String createdOn) {
		SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date d = null;
		try {
			d = sd.parse(createdOn);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.createdOn = new Date(d.getTime());

	}

	public Date getLastModifiedOn() {
		return lastModiedOn;
	}

	public void setLastModifiedOn(String lastModiedOn) {
		SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date d = null;
		try {
			d = sd.parse(lastModiedOn);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.lastModiedOn = new Date(d.getTime());
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String departmentId) {
		this.department = departmentId;
	}

	public Date getLastAccessedOn() {
		return lastAccessedOn;
	}

	public void setLastAccessedOn(Date lastAccessedOn) {
		this.lastAccessedOn = lastAccessedOn;
	}

	public String getLastAccessedBy() {
		return lastAccessedBy;
	}

	public void setLastAccessedBy(String lastAccessedBy) {
		this.lastAccessedBy = lastAccessedBy;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
