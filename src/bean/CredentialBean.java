package bean;

public class CredentialBean {
	String userId;
	String password;
	String userType;
	byte loginStatus;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public byte getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(byte loginStatus) {
		this.loginStatus = loginStatus;
	}
	

}
