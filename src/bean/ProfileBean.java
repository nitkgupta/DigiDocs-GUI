package bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ProfileBean {
	String userId;
	String firstName;
	String lastName;
	Date dateOfBirth;
	String emailId;
	String contactNumber;
	String address;
	String departmentId;
	String gender;
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		SimpleDateFormat sd = new SimpleDateFormat("dd-mm-yyyy");
		java.util.Date d = null;
		try {
			d = sd.parse(dateOfBirth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.dateOfBirth = new Date(d.getTime());
	}
	public void setDateOfBirth1(Date dateOfBirth) {
		this.dateOfBirth=dateOfBirth;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
}
