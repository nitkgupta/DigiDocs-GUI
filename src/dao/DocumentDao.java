package dao;

import java.awt.Desktop;
import java.awt.Window;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import bean.DocumentBean;
import con1.DatabaseCon;

public class DocumentDao {
	Connection con = DatabaseCon.getConnection();
	PreparedStatement ps;
	public String createDocument(DocumentBean bean ) {
		
		String id = generateDocumentId(bean.getDocumentName());
		java.util.Date d = new java.util.Date();
		java.sql.Date createdOn = new Date(d.getTime());
		
		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement("insert into document values(?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, id);
			ps.setString(2, bean.getDocumentName());
			ps.setInt(3, 0);
			ps.setString(4, bean.getCreatedByUser());
			ps.setDate(5, createdOn);
			ps.setDate(6, createdOn);
			ps.setString(7, bean.getDepartment());
			ps.setDate(8, createdOn);
			ps.setString(9, bean.getCreatedByUser());
			ps.setString(10, bean.getCategory());
			int a = ps.executeUpdate();
			if (a > 0 ) {
				con.commit();
				return id;
			} else {
				return "FAIL";
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return "FAIL";
	}

	public int deleteDocument(ArrayList<String> li) {
		int count = 0;
		try {
			for (String id : li) {
				ps = con.prepareStatement("Delete from document where documentId = ?");
				ps.setString(1, id);
				int r1 = ps.executeUpdate();
				count = count + r1;
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	public boolean updateDocumentName(String documentId, String newName) {
		try {
			ps = con.prepareStatement("Update document  set documentName =? where documentId=?");

			ps.setString(1, newName);
			ps.setString(2, documentId);
			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean updateDocumentSize(String documentId, int newSize) {
		try {
			ps = con.prepareStatement("Update document  set size =? where documentId=?");

			ps.setInt(1, newSize);
			ps.setString(2, documentId);
			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean updateLastAccessedBy(String documentId, String newName) {
		try {
			ps = con.prepareStatement("Update document  set lastAccessedBy = ? where documentId=?");

			ps.setString(1, newName);
			ps.setString(2, documentId);
			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean updateLastModifiedOn(String documentId) {
		java.util.Date d = new java.util.Date();
		java.sql.Date modifiedToday = new Date(d.getTime());
		try {
			ps = con.prepareStatement("Update document  set lastModifiedOn = ? where documentId=?");

			ps.setDate(1, modifiedToday);
			ps.setString(2, documentId);
			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean updateLastAccessedOn(String documentId) {
		java.util.Date d = new java.util.Date();
		java.sql.Date accessedToday = new Date(d.getTime());
		try {
			ps = con.prepareStatement("Update document  set lastAccessedOn = ? where documentId=?");

			ps.setDate(1, accessedToday);
			ps.setString(2, documentId);
			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public String generateDocumentId(String name) {
		String id = "";
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String month = Integer.toString(now.get(Calendar.MONTH)+1);
		System.out.println(month);
		if (month.length() == 1)
			month = "0" + month;
		int day = now.get(Calendar.DATE);
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select seqDocumentId from sequences");
			rs.next();
			String seq = rs.getString(1);
			id = name.substring(0, 2) + year + month + day + seq;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;

	}

	public DocumentBean findById(String documentId) {
		try {
			ps = con.prepareStatement("Select * from document where documentId=?");
			ps.setString(1, documentId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				DocumentBean bean = new DocumentBean();
				bean.setDocumentId(rs.getString(1));
				bean.setDocumentName(rs.getString(2));
				bean.setSize(rs.getInt(3));
				bean.setCreatedByUser(rs.getString(4));
				bean.setCreatedOn(rs.getString(5));
				bean.setLastModifiedOn(rs.getString(6));
				bean.setDepartment(rs.getString(7));

				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<DocumentBean> findAll() {
		ArrayList<DocumentBean> li = new ArrayList<DocumentBean>();
		try {
			ps = con.prepareStatement("Select * from document");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DocumentBean bean = new DocumentBean();
				bean.setDocumentId(rs.getString(1));
				bean.setDocumentName(rs.getString(2));
				bean.setSize(rs.getInt(3));
				bean.setCreatedByUser(rs.getString(4));
				bean.setCreatedOn(rs.getString(5));
				bean.setLastModifiedOn(rs.getString(6));
				bean.setDepartment(rs.getString(7));

				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	}
	
	public boolean uploadDocumentImage(String documentId  , String absolutePath) throws IOException
	{
		boolean flag = false;
		try {
			File f = new File(absolutePath);
			FileInputStream fis = new FileInputStream(f);
			ps = con.prepareStatement("insert into inputs values(?,?)");
			ps.setString(1, documentId);
			ps.setBinaryStream(2, (InputStream)fis , (int)f.length());
			
			int i = ps.executeUpdate();
			con.commit();
			fis.close();
			ps.close();
			
			if(i > 0)
			{
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
		return flag;
		
	}
	
	public boolean saveDocumentOutput(String documentId , String absolutePath) throws IOException
	{
		boolean flag = false;
		try {
			File f = new File(absolutePath);
			int size = (int)f.length();
			FileInputStream fis = new FileInputStream(f);
			ps = con.prepareStatement("insert into outputs values(?,?)");
			ps.setString(1, documentId);
			System.out.println(documentId);
			System.out.println(fis);
			ps.setBinaryStream(2, (InputStream)fis , size);
			int i = ps.executeUpdate();
			con.commit();
			fis.close();
			ps.close();
			if(i > 0 && updateDocumentSize(documentId, size))
			{
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	public int getDocumentImage(String documentId)
	{
		int result = 0;
		PreparedStatement ps;
		try {
			ps = con.prepareStatement("select image from inputs where documentId = ?");
			ps.setString(1, documentId);
//			ps.setString(1, "Ni20180324116");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String stu="F:\\DigiDocs-Docs\\new.jpg";
				InputStream is = rs.getBinaryStream(1);
				FileOutputStream fos = new FileOutputStream(new File(stu));
				int c = 0;
				while((c = is.read() ) >  -1)
				{
					fos.write(c);
				}
				fos.flush();
				fos.close();
				Desktop desk=Desktop.getDesktop();
				File f=new File(stu);
				try {
					desk.open(f);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result++;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String getDocumentDoc(String documentId)
	{
		String result = null;
		try {
			ps = con.prepareStatement("select doc from outputs where documentId = ?");
			ps.setString(1, documentId);
			
			ResultSet rs = ps.executeQuery();
			String stu="F:\\DigiDocs-Docs\\new.doc";
			if(rs.next())
			{
				InputStream is = rs.getBinaryStream(1);
				
				FileOutputStream fos = new FileOutputStream(new File(stu));
				int c = 0;
				while((c = is.read() ) > -1)
				{
					fos.write(c);
				}
				fos.flush();
				fos.close();
				Desktop desk=Desktop.getDesktop();
				File f=new File(stu);
				try {
					desk.open(f);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result =  "SUCCESS";
			}
			else
			{
				result =  "NO SUCH DOCUMENT";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<DocumentBean> getList()
	{
		ArrayList<DocumentBean> li = new ArrayList<DocumentBean>();
		try {
			ps = con.prepareStatement("Select documentId , documentName , createdByUser , CreatedOn from document");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DocumentBean bean = new DocumentBean();
				bean.setDocumentId(rs.getString(1));
				bean.setDocumentName(rs.getString(2));
				bean.setCreatedByUser(rs.getString(3));
				bean.setCreatedOn(rs.getString(4));
				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	
	}
	public ArrayList<DocumentBean> findByDepartment(String departmentId) {
		ArrayList<DocumentBean> li = new ArrayList<DocumentBean>();
		try {
			ps = con.prepareStatement("Select * from document where department = ?");
			ps.setString(1, departmentId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DocumentBean bean = new DocumentBean();
				bean.setDocumentId(rs.getString(1));
				bean.setDocumentName(rs.getString(2));
				bean.setSize(rs.getInt(3));
				bean.setCreatedByUser(rs.getString(4));
				bean.setCreatedOn(rs.getString(5));
				bean.setLastModifiedOn(rs.getString(6));
				bean.setDepartment(rs.getString(7));

				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	}
}
	
	
	
