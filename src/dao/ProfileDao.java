package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.ProfileBean;
import con1.DatabaseCon;

public class ProfileDao {
	Connection con = DatabaseCon.getConnection();

	public boolean createProfile(ProfileBean bean) {
		PreparedStatement prepareStatement;
		try {
			con.setAutoCommit(false);
			prepareStatement = con.prepareStatement("insert into profile values(?,?,?,?,?,?,?,?,?)");
			prepareStatement.setString(1, bean.getUserId());
			prepareStatement.setString(2, bean.getFirstName());
			prepareStatement.setString(3, bean.getLastName());
			prepareStatement.setDate(4, bean.getDateOfBirth());
			prepareStatement.setString(5, bean.getContactNumber());
			prepareStatement.setString(6, bean.getAddress());
			prepareStatement.setString(7, bean.getDepartmentId());
			prepareStatement.setString(8, bean.getEmailId());
			prepareStatement.setString(9, bean.getGender());
			int a = prepareStatement.executeUpdate();
			if (a > 0) {
				con.commit();
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public int deleteProfile(ArrayList<String> li) {
		int count = 0;
		try {
			for (String id : li) {
				PreparedStatement st = con.prepareStatement("Delete from profile where userId = ?");
				st.setString(1, id);
				int r1 = st.executeUpdate();
				count = count + r1;
				System.out.println(r1);
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	public boolean updateProfile(ProfileBean bean) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement(
					"Update profile set firstName=?, lastName=?,  contactNumber=?, address=?, departmentId=?, emailId=?,gender=?,dateOfBirth=? where userId=?");

			preparedStatement.setString(1, bean.getFirstName());
			preparedStatement.setString(2, bean.getLastName());
			preparedStatement.setString(3, bean.getContactNumber());
			preparedStatement.setString(4, bean.getAddress());
			preparedStatement.setString(5, bean.getDepartmentId());
			preparedStatement.setString(6, bean.getEmailId());
			preparedStatement.setString(7, bean.getGender());
			preparedStatement.setDate(8, bean.getDateOfBirth());
			preparedStatement.setString(9, bean.getUserId());
			
			int r1 = preparedStatement.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;

	}

	/*----not required---
	 * public String generateProfileId(String name)
	{
		String id = "";
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select seqProfileId from sequences");
			String seq  = rs.getString(1);
			rs.next();
			id = name.substring(0, 3)+"@"+seq;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
		
	}*/
	public ProfileBean findById(String userId) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from profile where userId=?");
			preparedStatement.setString(1, userId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				ProfileBean bean = new ProfileBean();
				bean.setUserId(rs.getString(1));
				bean.setFirstName(rs.getString(2));
				bean.setLastName(rs.getString(3));
				bean.setDateOfBirth(rs.getString(4));
				bean.setContactNumber(rs.getString(5));
				bean.setAddress(rs.getString(6));
				bean.setDepartmentId(rs.getString(7));
				bean.setEmailId(rs.getString(8));

				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<ProfileBean> findAll() {
		ArrayList<ProfileBean> li = new ArrayList<ProfileBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from profile");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				ProfileBean bean = new ProfileBean();
				bean.setUserId(rs.getString(1));
				bean.setFirstName(rs.getString(2));
				bean.setLastName(rs.getString(3));
				bean.setDateOfBirth(rs.getString(4));
				bean.setContactNumber(rs.getString(5));
				bean.setAddress(rs.getString(6));
				bean.setDepartmentId(rs.getString(7));
				bean.setEmailId(rs.getString(8));

				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	}
	
	public ProfileBean findNameDept(String userId) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select firstName , departmentId from profile where userId=?");
			preparedStatement.setString(1, userId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				ProfileBean bean = new ProfileBean();
				bean.setFirstName(rs.getString(1));
				bean.setDepartmentId(rs.getString(2));

				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}
}
