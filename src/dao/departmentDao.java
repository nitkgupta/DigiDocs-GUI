package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.DepartmentBean;
import con1.DatabaseCon;
import util.Repository;

public class departmentDao {
	Connection con = DatabaseCon.getConnection();
	PreparedStatement ps;
	public String createDepartment(DepartmentBean bean) {
		
		String id = generateDepartmentId(bean.getDepartmentName());
		try {
			ps = con.prepareStatement("insert into department values(?,?,?)");
			ps.setString(1, id);
			ps.setString(2, bean.getDepartmentName());
			ps.setString(3, bean.getDepartmentKey());

			int a = ps.executeUpdate();
			if (a > 0 && Repository.createDepartmentDirectory(id)) {
				System.out.println("here");
				return id;
			} else {
				return "FAIL";
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return "FAIL";
	}

	public int deleteDepartment(ArrayList<String> li) {
		int count = 0;
		try {
			for (String id : li) {
				ps = con.prepareStatement("Delete from department where departmentId = ?");
				ps.setString(1, id);
				int r1 = ps.executeUpdate();
				count = count + r1;
				System.out.println(r1);
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	public boolean updateDepartmentName(DepartmentBean bean, String newName) {
		try {
			ps = con.prepareStatement("Update department set departmentName=?   where departmentId=?");

			ps.setString(1, newName);
			ps.setString(2, bean.getDepartmentId());

			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean updateDepartmentKey(DepartmentBean bean, String newKey) {
		try {
			ps = con.prepareStatement("Update department set departmentKey=?   where departmentId=? ");

			ps.setString(1, newKey);
			ps.setString(2, bean.getDepartmentId());

			int r1 = ps.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public boolean checkDepartmentKey(DepartmentBean bean) {
		try {
			 ps = con.prepareStatement("select departmentId from department where departmentId = ? and departmentKey = ?");
			ps.setString(1, bean.getDepartmentId());
			ps.setString(2, bean.getDepartmentKey());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}

	public String generateDepartmentId(String name) {
		String id = "";
		System.out.println("dsbgferdfsvgt");
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select seqDepartmentId from sequences");
			rs.next();
			String seq = rs.getString(1);
			System.out.println(seq);
			id = name.substring(0, 2) + seq;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;

	}

	public DepartmentBean findById(String departmentId) {
		try {
			ps = con.prepareStatement("Select * from department where departmentId=?");
			ps.setString(1, departmentId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				DepartmentBean bean = new DepartmentBean();
				bean.setDepartmentId(rs.getString(1));
				bean.setDepartmentName(rs.getString(2));
				bean.setDepartmentKey(rs.getString(3));

				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;

	}

	/*
	 * public boolean createDirectory(String id) { boolean created = false; File
	 * f = new File("LocalRepository/"+id); if(!f.exists()) { created =
	 * f.mkdir(); } return created;
	 * 
	 * }
	 */
	public ArrayList<DepartmentBean> findAll() {
		ArrayList<DepartmentBean> li = new ArrayList<DepartmentBean>();
		try {
			ps = con.prepareStatement("Select * from department");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DepartmentBean bean = new DepartmentBean();
				bean.setDepartmentId(rs.getString(1));
				bean.setDepartmentKey(rs.getString(3));
				bean.setDepartmentName(rs.getString(2));

				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	}

}
