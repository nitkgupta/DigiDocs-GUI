package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.CredentialBean;
import bean.ProfileBean;
import con1.DatabaseCon;
import services.ProfileVaild;

public class CredentialDao {
	Connection con  = DatabaseCon.getConnection();
	
		public String createCredential( CredentialBean bean , ProfileBean pbean)
		{
			
			PreparedStatement prepareStatement;
			String id = generateUserId(pbean.getFirstName());
			
			try {
				con.setAutoCommit(false);
				prepareStatement = con.prepareStatement("insert into credentials values(?,?,?,?)");
				prepareStatement.setString(1, id);
				prepareStatement.setString(2, bean.getPassword());
				prepareStatement.setByte(3, (byte) 0);
				prepareStatement.setString(4, "u");
				int a =  prepareStatement.executeUpdate();
				if (a > 0) {
					pbean.setUserId(id);
					ProfileVaild pv = new ProfileVaild();
					if(pv.createProfile(pbean))
					{
						con.commit();
						return id;
					}
					
					
				} else {
					return "FAIL";
				}
				
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
			return "FAIL";
			
		}
		
		public int deleteCredential(ArrayList<String> li) {
			int count = 0;
			try {
				for (String id : li) {
					PreparedStatement st = con.prepareStatement("Delete from credentials where userId = ?");
					st.setString(1, id);
					int r1 = st.executeUpdate();
					count = count + r1;
				}
				return count;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return count;
		}
		
		
		public boolean changePassword(CredentialBean bean , String newPassword) {
			try {
				PreparedStatement preparedStatement = con
						.prepareStatement("Update credentials  set password=? where userId=? and password = ?");
				preparedStatement.setString(1, newPassword);
				preparedStatement.setString(2, bean.getUserId());
				preparedStatement.setString(3, bean.getPassword());
				
				int r1 = preparedStatement.executeUpdate();
				if (r1 > 0) {
					return true;

				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
			return false;
		}
		
		
		public boolean changeLoginStatus(String userId , byte newLoginStatus) {
			try {
				PreparedStatement preparedStatement = con
						.prepareStatement("Update credentials  set loginStatus=? where userId=?");

				preparedStatement.setByte(1, newLoginStatus);
				preparedStatement.setString(2, userId);
				int r1 = preparedStatement.executeUpdate();
				if (r1 > 0) {
					return true;

				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
			return false;
		}
		
		public String generateUserId(String name)
		{
			String id = null;
			try {
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select seqUserId from sequences");
				rs.next();
				String seq  = rs.getString(1);				
				id = name.substring(0, 3)+"@"+seq;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return id;
		}
		
		public CredentialBean findById(String userId) {
			try {
				PreparedStatement preparedStatement = con.prepareStatement("Select * from credentials where userId=?");
				preparedStatement.setString(1, userId);
				ResultSet rs = preparedStatement.executeQuery();
				if (rs.next()) {
					CredentialBean bean = new CredentialBean();
					bean.setUserId(rs.getString(1));
					bean.setPassword(rs.getString(2));
					bean.setLoginStatus(rs.getByte(3));
					bean.setUserType(rs.getString(4));
					
					return bean;
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
			return null;
		}
		
		
		public ArrayList<CredentialBean> findAll() {
			ArrayList<CredentialBean> li = new ArrayList<CredentialBean>();
			try {
				PreparedStatement preparedStatement = con.prepareStatement("Select * from credentials");
				ResultSet rs = preparedStatement.executeQuery();
				while (rs.next()) {
					CredentialBean bean = new CredentialBean();
					bean.setUserId(rs.getString(1));
					bean.setPassword(rs.getString(2));
					bean.setLoginStatus(rs.getByte(3));
					bean.setUserType(rs.getString(4));
					
					li.add(bean);
				}
			}

			catch (SQLException e) {
				e.printStackTrace();
			}

			return li;
		}
		
		
		

}
