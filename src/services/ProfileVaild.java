package services;

import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import bean.ProfileBean;
import dao.ProfileDao;

public class ProfileVaild {
	Pattern p = Pattern.compile("(\\w){3}+@(\\d){4}");
	Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	private boolean  isValid = false;
	ProfileDao pd = new ProfileDao();
	public boolean createProfile(ProfileBean profilebean){
		
		String lastName  = profilebean.getLastName();
		Date dateOfBirth =  profilebean.getDateOfBirth();
		String contactNumber = profilebean.getContactNumber();
		String address =  profilebean.getAddress();
		String emailId =  profilebean.getEmailId();
//		if(lastName !=null && dateOfBirth !=null && contactNumber !=null && address !=null && emailId !=null) 
		if(lastName.length()<= 25 )
		{
		Matcher e = email.matcher(emailId);
		if( e.matches())
		{
			isValid = true;
		}
		}
		
		if(isValid)
		{
			return pd.createProfile(profilebean);
		}
				
			
		return isValid;
	}
	public int deleteProfile(ArrayList<String> userId) {
		ProfileDao profileDao = new ProfileDao();
		for(int i=0;i<userId.size();i++)
		{
			Matcher m = p.matcher(userId.get(i));
			if(m.matches())
			{
				isValid = true;
			}
		}
		if(isValid)
		{
			return profileDao.deleteProfile(userId);
		}
		else 
		{
		return 0;
		}
	}
	public boolean updateProfile(ProfileBean profilebean){
		if(profilebean!=null) {
			String userId = profilebean.getUserId();
			String  firstName = profilebean.getFirstName();
			String lastName  = profilebean.getLastName();
			String contactNumber = profilebean.getContactNumber();
			String address =  profilebean.getAddress();
			String emailId =  profilebean.getEmailId();
			String departmentId = profilebean.getDepartmentId();
//			if(userId !=null && firstName !=null && lastName !=null  && contactNumber !=null && address !=null && emailId !=null &&  departmentId != null) 
				if(firstName.length() <=25 && lastName.length()<= 25 && departmentId.matches("(\\w){2}(\\d){3}") )
				{
					Matcher m = p.matcher(userId);
					Matcher e = email.matcher(emailId);
					if(m.matches() && e.matches())
					{
						isValid = true;
					}
				}
				if(isValid)
				{
					ProfileDao profileDao = new ProfileDao();
					return profileDao.updateProfile(profilebean);
				}
			}
			return isValid;
		}
	public ProfileBean viewProfile(String userId) {
		ProfileDao profileDao = new ProfileDao();
		Matcher m = p.matcher(userId);
		if(userId!=null  && m.matches() )
		return profileDao.findById(userId);
		else 
			return null;
	}
	
	public ProfileBean getNameDept(String userId) {
		ProfileDao profileDao = new ProfileDao();
		Matcher m = p.matcher(userId);
		if(userId!=null  && m.matches() )
		return profileDao.findNameDept(userId);
		else 
			return null;
	}
}



