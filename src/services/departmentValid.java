package services;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bean.DepartmentBean;
import dao.departmentDao;

public class departmentValid {
	private boolean isValid = false;
	Pattern p = Pattern.compile("(\\w){2}(\\d){3}");
	private departmentDao departmentDao = new departmentDao();

	public String createDepartment(DepartmentBean departmentbean) {

		String departmentName = departmentbean.getDepartmentName();
		String departmentKey = departmentbean.getDepartmentKey();

		if (departmentName != null && departmentKey != null)
			if (departmentName.length() >= 2 && departmentName.length() <= 25 && departmentKey.length() > 0
					&& departmentKey.length() <= 45) {
				isValid = true;
			}

		if (isValid) {
			return departmentDao.createDepartment(departmentbean);
		}

		else
			return "INVALID";
	}

	public int deleteDepartment(ArrayList<String> departmentId) {

		for (int i = 0; i < departmentId.size(); i++)

			if (departmentId.get(i) != null && departmentId.get(i).matches("(\\w){2}(\\d){3}")) {

				isValid = true;
			}

		if (isValid) {

			return departmentDao.deleteDepartment(departmentId);
		} else
			return 0;

	}

	public boolean updateDepartmentName(DepartmentBean departmentbean, String newName) {

		if (departmentbean != null) {
			String departmentId = departmentbean.getDepartmentId();

			if (departmentId != null)
				if (departmentId.length() <= 8) {
					Matcher m = p.matcher(departmentId);

					if (m.matches()) {
						isValid = true;
					}
				}
			if (isValid) {

				return departmentDao.updateDepartmentName(departmentbean, newName);
			}
		}
		return isValid;
	}

	public boolean updateDepartmentKey(DepartmentBean departmentbean, String newKey) {

		if (departmentbean != null) {
			String departmentId = departmentbean.getDepartmentId();

			if (departmentId != null && newKey != null)
				if (departmentId.length() <= 8 && newKey.length() > 0 && newKey.length() <= 45) {
					Matcher m = p.matcher(departmentId);
					if (m.matches()) {
						isValid = true;

					}
				}
			if (isValid) {

				return departmentDao.updateDepartmentKey(departmentbean, newKey);
			}
		}
		return isValid;
	}

	public DepartmentBean viewDepartment(String departmentId) {

		if (departmentId != null && departmentId.matches("(\\w){2}(\\d){3}"))
			return departmentDao.findById(departmentId);
		else
			return null;
	}

}
