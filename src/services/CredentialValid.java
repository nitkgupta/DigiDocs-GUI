package services;

import java.util.ArrayList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bean.CredentialBean;
import bean.ProfileBean;
import dao.CredentialDao;

public class CredentialValid {
	Pattern p = Pattern.compile("(\\w){3}+@(\\d){4}");
	private boolean isValid = false;
	public String registerUser(CredentialBean credentialBean , ProfileBean pbean) {
		
		CredentialDao credentialDao = new CredentialDao();
		String password = credentialBean.getPassword();
		String fname = pbean.getFirstName();
		if( password !=null && fname !=null  )
		if( password.length()<= 15 && password.length() > 6 && fname.length() >3 && fname.length() <= 25){
			{
			return credentialDao.createCredential(credentialBean,pbean);
			}
	
		}
		return "INVALID";
		
	}
	
	public int deleteCredential(ArrayList<String> userId) {
		CredentialDao credentialDao = new CredentialDao();
		for(int i=0;i<userId.size();i++)
		{
			Matcher m = p.matcher(userId.get(i));
			if(m.matches())
			{
				
				isValid = true;
			}
		}
		if(isValid)
		{
			
			return credentialDao.deleteCredential(userId);
		}
		else 
		{
			
		return 0;
		}
	}



public boolean changePasseord(CredentialBean credentialBean , String newPassword){
	if(credentialBean!=null) {
		String userId = credentialBean.getUserId();
		String password = credentialBean.getPassword();
		
		if(userId !=null && password !=null )
		if(userId.length() <=8 && password.length()<= 15 )
			{
				Matcher m = p.matcher(userId);
				
				if(m.matches())
				{
					isValid = true;
				}
			}
			if(isValid)
			{
				CredentialDao credentialDao = new CredentialDao();
				return credentialDao.changePassword(credentialBean , newPassword);
			}
		}
		return isValid;
	}

public boolean changeLoginStatus(String userId , byte newLoginStatus){
	
		
		if(userId !=null  )
		if(userId.length() <=8 && (newLoginStatus == 0 || newLoginStatus == 1) )
			{
				Matcher m = p.matcher(userId);
				
				if(m.matches())
				{
					isValid = true;
				}
			}
			if(isValid)
			{
				CredentialDao credentialDao = new CredentialDao();
				return credentialDao.changeLoginStatus(userId , newLoginStatus);
			}
		
		return isValid;
	}
public CredentialBean viewCredential(String userId) {
	CredentialDao credentialDao = new CredentialDao();
	Matcher m =p.matcher(userId);
	if(userId!=null  &&  m.matches())
	return credentialDao.findById(userId);
	else 
		return null;
}

}
