package services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bean.DocumentBean;

import dao.DocumentDao;

public class DocumentValid {
	Pattern p = Pattern.compile("(\\w){2}(\\d){11}");
	private boolean isValid = false;

	public String createDocument(DocumentBean documentBean ) {
		DocumentDao documentDao = new DocumentDao();
		
		String documentName = documentBean.getDocumentName();
		String createdBy = documentBean.getCreatedByUser();
		String dept = documentBean.getDepartment();
		String category = documentBean.getCategory();
		if (documentName != null  && createdBy != null && category != null && dept != null)
			if (documentName.length() <= 45) {
				isValid = true;
			} 

		if (isValid) {
			return documentDao.createDocument(documentBean );
		}

		return "INVALID";

	}

	public int deleteDocument(ArrayList<String> documentId) {
		DocumentDao documentDao = new DocumentDao();
		for (int i = 0; i < documentId.size(); i++) {
			Matcher m = p.matcher(documentId.get(i));

			if (m.matches()) {
				isValid = true;
			}
		}
		if (isValid) {
			return documentDao.deleteDocument(documentId);
		} else
			return 0;

	}

	public boolean updateDocumentSize(String documentId, int newSize) {

		if (newSize != 0) {
			Matcher m = p.matcher(documentId);
			if (m.matches()) {
				isValid = true;
			}
		}
		if (isValid) {
			DocumentDao documentDao = new DocumentDao();
			return documentDao.updateDocumentSize(documentId, newSize);
		}

		return isValid;
	}

	public boolean updateDocumentName(String documentId, String newName) {

		if (newName != null) {
			Matcher m = p.matcher(documentId);
			if (m.matches()) {
				isValid = true;
			}
		}
		if (isValid) {
			DocumentDao documentDao = new DocumentDao();
			return documentDao.updateDocumentName(documentId, newName);
		}

		return isValid;
	}

	public boolean updateLastAccessedBy(String documentId, String newName) {

		if (newName != null && documentId != null) {
			Matcher m = p.matcher(documentId);
			if (m.matches()) {
				isValid = true;
			}
		}
		if (isValid) {
			DocumentDao documentDao = new DocumentDao();
			return documentDao.updateLastAccessedBy(documentId, newName);
		}

		return isValid;
	}

	public boolean updateLastModifiedOn(String documentId) {

		if (documentId != null) {
			Matcher m = p.matcher(documentId);
			if (documentId != null && m.matches()) {
				isValid = true;
			}
		}

		if (isValid) {
			DocumentDao documentDao = new DocumentDao();
			return documentDao.updateLastModifiedOn(documentId);
		}

		return isValid;
	}

	public boolean updateLastAccessedOn(String documentId) {

		if (documentId != null) {
			Matcher m = p.matcher(documentId);
			if (m.matches()) {
				isValid = true;
			}
		}

		if (isValid) {
			DocumentDao documentDao = new DocumentDao();
			return documentDao.updateLastAccessedOn(documentId);
		}

		return isValid;
	}

	public DocumentBean viewDocument(String documentId) {
		DocumentDao documentDao = new DocumentDao();
		Matcher m = p.matcher(documentId);
		if (documentId != null && m.matches())
			return documentDao.findById(documentId);
		else
			return null;

	}
	
	public boolean uploadDocumentImage(String documentId  , String absolutePath) throws IOException
	{
		DocumentDao documentDao = new DocumentDao();
		Matcher m = p.matcher(documentId);
		if (documentId != null && m.matches() && absolutePath != null && absolutePath.length()>0)
			return documentDao.uploadDocumentImage(documentId, absolutePath);
		else
			return false;
	}
	
	public boolean saveDocumentOutput(String documentId  , String absolutePath) throws IOException
	{
		DocumentDao documentDao = new DocumentDao();
		Matcher m = p.matcher(documentId);
		if (documentId != null && m.matches() && absolutePath != null && absolutePath.length()>0)
			return documentDao.saveDocumentOutput(documentId, absolutePath);
		else
			return false;
	}
	
	public int getDocumentImage(String documentId )
	{
		DocumentDao documentDao = new DocumentDao();
		Matcher m = p.matcher(documentId);
		if(documentId != null)
		{
			if(m.matches() )
			{
				return documentDao.getDocumentImage(documentId);
			}
		}
		return 0;
			
	}
	
	public String getDocumentDoc(String documentId)
	{
		DocumentDao documentDao = new DocumentDao();
		Matcher m = p.matcher(documentId);
		if(documentId != null)
		{
			if(m.matches() )
			{
				return documentDao.getDocumentDoc(documentId);
			}
		}
		return null;
			
	}
	public ArrayList<DocumentBean> viewByDepartment(String departmentId) {
		DocumentDao documentDao = new DocumentDao();
		if (departmentId != null && departmentId.matches("(\\w){2}(\\d){3}"))
			return documentDao.findByDepartment(departmentId);
		else
			return null;

	}


	public ArrayList<DocumentBean> viewAll()
	{
		DocumentDao documentDao = new DocumentDao();
		return documentDao.findAll();
	}
}
