package usercom;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import com.toedter.calendar.JDateChooser;

import bean.CredentialBean;
import bean.ProfileBean;
import dao.ProfileDao;
import services.ProfileVaild;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import javax.swing.ImageIcon;

public class Profile extends JFrame {

	private JPanel contentPane;
	private JTextField fname;
	private JTextField lname;
	private JTextField email;
	private JTextField contact;
	private JOptionPane optionpane;
	private JComboBox comboBox2;
	static public String id;
	static public String nm;
	static public String dep;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Profile frame = new Profile(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("Update Profile");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Profile(CredentialBean bean,ProfileBean pb) {
		id=bean.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080,650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1064, 621);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(51, 204, 255));
		panel_1.setBounds(0, 0, 532, 621);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblProfileUpdate = new JLabel("Profile Update");
		lblProfileUpdate.setFont(new Font("HP Simplified", Font.BOLD, 36));
		lblProfileUpdate.setForeground(new Color(0, 0, 0));
		lblProfileUpdate.setBounds(154, 34, 234, 74);
		panel_1.add(lblProfileUpdate);
		
		JLabel lblNewLabel_3 = new JLabel(" ");
		lblNewLabel_3.setIcon(new ImageIcon(Profile.class.getResource("/com/prf-upd.png")));
		lblNewLabel_3.setBounds(135, 195, 272, 257);
		panel_1.add(lblNewLabel_3);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 255, 255));
		panel_2.setBounds(530, 0, 534, 621);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblFirstName.setBounds(65, 33, 134, 31);
		panel_2.add(lblFirstName);
		
		JLabel lblNewLabel = new JLabel("Last Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setBounds(65, 81, 134, 31);
		panel_2.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Email ID");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel_1.setBounds(65, 130, 134, 31);
		panel_2.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Contact");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel_2.setBounds(65, 177, 134, 31);
		panel_2.add(lblNewLabel_2);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblAddress.setBounds(65, 221, 134, 31);
		panel_2.add(lblAddress);
		
		JLabel lblDateOfBirth = new JLabel("Date of Birth");
		lblDateOfBirth.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDateOfBirth.setBounds(65, 357, 134, 31);
		panel_2.add(lblDateOfBirth);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDepartment.setBounds(65, 399, 134, 31);
		panel_2.add(lblDepartment);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(238, 66, 227, 2);
		panel_2.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(238, 116, 227, 2);
		panel_2.add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(238, 163, 227, 2);
		panel_2.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(238, 210, 227, 2);
		panel_2.add(separator_3);
		
		fname = new JTextField();
		fname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fname.setBounds(238, 33, 228, 31);
		panel_2.add(fname);
		fname.setColumns(10);
		fname.setBorder(null);
		
		lname = new JTextField();
		lname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lname.setBounds(236, 81, 229, 33);
		panel_2.add(lname);
		lname.setColumns(10);
		lname.setBorder(null);
		
		email = new JTextField();
		email.setFont(new Font("Tahoma", Font.PLAIN, 14));
		email.setColumns(10);
		email.setBounds(238, 130, 228, 31);
		panel_2.add(email);
		email.setBorder(null);
		
		contact = new JTextField();
		contact.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contact.setColumns(10);
		contact.setBounds(238, 177, 228, 31);
		panel_2.add(contact);
		contact.setBorder(null);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBackground(new Color(255, 255, 255));
		dateChooser.setBounds(238, 357, 229, 31);
		panel_2.add(dateChooser);
		dateChooser.setBorder(null);
		
		JButton button2 = new JButton("<");
		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Welcome we=new Welcome(cb,pb);
				setVisible(false);
				we.setVisible(true);
			}
		});
		button2.setForeground(new Color(255, 255, 255));
		button2.setBackground(Color.DARK_GRAY);
		button2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button2.setBounds(10, 11, 59, 41);
		panel_1.add(button2);
		
		String[] choices = { "ab106","bi110", "ci109","co101","el108","me107","me118","re111","an112"};
		JComboBox comboBox = new JComboBox(choices);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBox.setBackground(new Color(255, 255, 255));
		comboBox.setBounds(238, 399, 229, 31);
		panel_2.add(comboBox);
		comboBox.setBorder(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textArea.setRows(3);
		JScrollPane sp = new JScrollPane(textArea);
		sp.setBackground(new Color(255, 255, 255));
		sp.setForeground(new Color(160, 160, 160));
		sp.setBounds(238, 230, 227, 103);
		panel_2.add(sp);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
							 	
				Profile pr=new Profile(cb,pb);
				String fn=fname.getText();
				String ln=lname.getText();
				String em=email.getText();
				String cont=contact.getText();
				String address=textArea.getText();
				String gender=(String)comboBox2.getSelectedItem();
//				String date=dateChooser.getDate().toString();
				String dept=(String)comboBox.getSelectedItem();
				Date date=new Date(dateChooser.getDate().getTime());
				System.out.println(fn+ln+em+cont+address+dept+date);
				ProfileBean bean=new ProfileBean();
				bean.setFirstName(fn);
				bean.setLastName(ln);
				bean.setAddress(address);
				bean.setDepartmentId(dept);
				bean.setDateOfBirth1(date);
				bean.setContactNumber(cont);
				bean.setGender(gender);
				bean.setUserId(id);
				System.out.println(id);
				bean.setEmailId(em);
				ProfileVaild pf=new ProfileVaild();
				boolean check=pf.updateProfile(bean);
				optionpane=new JOptionPane();
				if(check) {
					optionpane.showMessageDialog(new Profile(cb,pb), "Profile Updated Successfully");
				}
				else {
					optionpane.setBackground(new Color(255, 255, 255));
					optionpane.setForeground(new Color(255, 255, 255));
					optionpane.showMessageDialog(new Profile(cb,pb), "Profile Updation Failed");
//					NitiSign3 n=new NitiSign3();
//					frame.setVisible(false);
//					n.setVisible(true);
				}

			}
		});
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setBackground(new Color(50, 204, 255));
		btnNewButton.setFont(new Font("Nirmala UI", Font.PLAIN, 21));
		btnNewButton.setBounds(121, 532, 250, 45);
		panel_2.add(btnNewButton);
		
		JLabel gend = new JLabel("Gender");
		gend.setFont(new Font("Tahoma", Font.PLAIN, 17));
		gend.setBounds(65, 447, 134, 31);
		panel_2.add(gend);
		
		String[] choices2= {"MALE","FEMALE","TRANSGENDER"};
		comboBox2 = new JComboBox(choices2);
		comboBox2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBox2.setBorder(null);
		comboBox2.setBackground(Color.WHITE);
		comboBox2.setBounds(238, 448, 229, 31);
		panel_2.add(comboBox2);
	}
}
