package usercom;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.layout.Document;

import bean.CredentialBean;
import bean.DocumentBean;
import bean.ProfileBean;
import dao.DocumentDao;
import services.DocumentValid;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;

public class AllDocs extends JFrame {

	private JPanel contentPane;
	private DefaultTableModel model;
	private JTable table_1;
	static String id;
	private JOptionPane pane;
	static ArrayList<DocumentBean> art;
	static ArrayList<DocumentBean> ar;
	static public String nm;
	static public String dep;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AllDocs frame = new AllDocs(new CredentialBean(),new ProfileBean(),art);
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("Documents");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")
	public AllDocs(CredentialBean cb,ProfileBean pb,ArrayList<DocumentBean> at ) {
		id = cb.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		ar=at;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setResizable(false);
		contentPane.setLayout(null);
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBounds(227, 0, 837, 621);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		table_1 = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		table_1.setBounds(60, 48, 723, 528);

		table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_1.setColumnSelectionAllowed(true);
		table_1.setBorder(null);
		table_1.setRowHeight(30);
		table_1.setBackground(new Color(255, 255, 255));
		table_1.getColumnModel().setColumnMargin(10);
		model = new DefaultTableModel(new Object[] { "Document Id", "Name", "Created By", "Created On" }, 0);
		table_1.setModel(model);
		JScrollPane js = new JScrollPane(table_1);
		js.setBounds(80, 46, 683, 391);
		js.setVisible(true);
		panel_1.add(js);
		DocumentBean bean;
		Iterator itr=ar.iterator();
		while(itr.hasNext()) {
			bean=(DocumentBean) itr.next();
			model.addRow(new Object[] { bean.getDocumentId(), bean.getDocumentName(), bean.getCreatedByUser(),bean.getCreatedOn() });
				
		}
		
		JButton btnNewButton = new JButton("Get Document");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("HP Simplified", Font.PLAIN, 16));
		btnNewButton.setBorder(null);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table_1.getSelectedRow();
				int column = table_1.getSelectedColumn();
				if (column != 0) {
					pane.showMessageDialog(null, "Please Select from Document Id only!!");
				} else {
					String value = table_1.getModel().getValueAt(row, column).toString();
					System.out.println(value);
					DocumentValid dd = new DocumentValid();
					dd.getDocumentDoc(value);
				}
			}
		});
		btnNewButton.setBackground(new Color(102, 153, 51));
		btnNewButton.setBounds(181, 488, 172, 43);
		panel_1.add(btnNewButton);

		JButton btnGetImage = new JButton("Get Image");
		btnGetImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table_1.getSelectedRow();
				int column = table_1.getSelectedColumn();
				if (column != 0) {
					pane.showMessageDialog(null, "Please Select from Document Id only!!");
				} else {
					String value = table_1.getModel().getValueAt(row, column).toString();
					System.out.println(value);
					DocumentValid dd = new DocumentValid();
					dd.getDocumentImage(value);
				}
			}
		});
		btnGetImage.setForeground(Color.WHITE);
		btnGetImage.setFont(new Font("HP Simplified", Font.PLAIN, 16));
		btnGetImage.setBorder(null);
		btnGetImage.setBackground(new Color(51, 51, 204));
		btnGetImage.setBounds(505, 488, 172, 43);
		panel_1.add(btnGetImage);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(new Color(51, 204, 255));
		panel.setBounds(0, 0, 227, 621);
		contentPane.add(panel);

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBackground(new Color(51, 204, 255));
		panel_2.setBounds(0, 0, 227, 187);
		panel.add(panel_2);

		JLabel label = new JLabel(nm);
		label.setForeground(Color.DARK_GRAY);
		label.setFont(new Font("HP Simplified", Font.PLAIN, 24));
		label.setBounds(76, 137, 117, 38);
		panel_2.add(label);

		JLabel label_1 = new JLabel(" ");
		label_1.setIcon(new ImageIcon(AllDocs.class.getResource("/com/refresh-516 new.png")));
		label_1.setBounds(56, 19, 110, 108);
		panel_2.add(label_1);

		JLabel lblAllDocuments = new JLabel("All Documents");
		lblAllDocuments.setForeground(new Color(0, 0, 0));
		lblAllDocuments.setFont(new Font("HP Simplified", Font.BOLD, 30));

		lblAllDocuments.setBounds(20, 284, 189, 61);
		panel.add(lblAllDocuments);

		JLabel lblAndImages = new JLabel("and Images");
		lblAndImages.setForeground(Color.BLACK);
		lblAndImages.setFont(new Font("HP Simplified", Font.BOLD, 30));
		lblAndImages.setBounds(32, 320, 157, 61);
		panel.add(lblAndImages);

		JButton btnReturn = new JButton("Return");
		btnReturn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb = new CredentialBean();
				cb.setUserId(id);
				Welcome we = new Welcome(cb,pb);

				setVisible(false);
				we.setVisible(true);
			}
		});
		btnReturn.setForeground(Color.WHITE);
		btnReturn.setFont(new Font("HP Simplified", Font.PLAIN, 16));
		btnReturn.setBorder(null);
		btnReturn.setBackground(Color.DARK_GRAY);
		btnReturn.setBounds(27, 552, 172, 43);
		panel.add(btnReturn);

	}
}
