package usercom;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.DocumentBean;
import bean.ProfileBean;
import dao.DocumentDao;
import services.DocumentValid;

import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import com.SignIn;

public class Welcome extends JFrame {

	private JPanel contentPane;
	static public String id;
	static public String nm;
	static public String dep;
	
//	public void set(CredentialBean bean) {
//		id=bean.getUserId();
//	}
 
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Welcome frame = new Welcome(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("Welcome");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Welcome(CredentialBean cd,ProfileBean pb) {
		id=cd.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080,650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 1064, 621);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(51, 204, 255));
		panel_1.setBounds(0, 0, 227, 621);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(51, 204, 255));
		panel_2.setBounds(0, 0, 227, 187);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Welcome "+ nm);
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("HP Simplified", Font.PLAIN, 20));
		lblNewLabel.setBounds(42, 138, 170, 38);
		panel_2.add(lblNewLabel);
		
		JLabel label = new JLabel(" ");
		label.setIcon(new ImageIcon(Welcome.class.getResource("/com/refresh-516 new.png")));
		label.setBounds(56, 19, 110, 108);
		panel_2.add(label);
		
		JButton btnNewButton = new JButton("Update Profile");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				setVisible(false);
				Profile pr=new Profile(cb,pb);
				pr.setVisible(true);

			}
		});
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setBackground(new Color(51, 204, 255));
		btnNewButton.setFont(new Font("Myriad Pro", Font.PLAIN, 19));
		btnNewButton.setBounds(0, 262, 227, 37);
		btnNewButton.setBorder(null);
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("View All Documents");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
//				Desktop desk=Desktop.getDesktop();
//				File f=new File("C:\\Users\\nitka\\Desktop\\JAWO\\Index.docx");
//				try {
//					desk.open(f);
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				DocumentValid ds=new DocumentValid();
				ArrayList<DocumentBean> al=ds.viewByDepartment(dep);
				AllDocs all=new AllDocs(cb,pb,al);
				setVisible(false);
				all.setVisible(true);

			}
		});
		btnNewButton_2.setFont(new Font("Myriad Pro", Font.PLAIN, 19));
		btnNewButton_2.setBackground(new Color(51, 204, 255));
		btnNewButton_2.setForeground(new Color(0, 0, 0));
		btnNewButton_2.setBounds(0, 354, 227, 37);
		btnNewButton_2.setBorder(null);
		panel_1.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Scan/Upload Document");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				
				Upload up=new Upload(cb,pb);
				setVisible(false);
				up.setVisible(true);

			}
		});
		btnNewButton_3.setFont(new Font("Myriad Pro", Font.PLAIN, 19));
		btnNewButton_3.setBackground(new Color(51, 204, 255));
		btnNewButton_3.setForeground(new Color(0, 0, 0));
		btnNewButton_3.setBounds(0, 308, 227, 37);
		btnNewButton_3.setBorder(null);
		panel_1.add(btnNewButton_3);
		
		JButton btnLogOut = new JButton("Log Out");
		btnLogOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				JOptionPane pn=new JOptionPane();
				int i=pn.showConfirmDialog(new Welcome(cb,pb), "Are you sure ?");
				if(i==0) {
				SignIn s=new SignIn();
				setVisible(false);
				s.setVisible(true); }
				
			}
		});
		btnLogOut.setForeground(Color.RED);
		btnLogOut.setFont(new Font("Myriad Pro", Font.PLAIN, 19));
		btnLogOut.setBorder(null);
		btnLogOut.setBackground(new Color(51, 204, 255));
		btnLogOut.setBounds(0, 543, 227, 37);
		panel_1.add(btnLogOut);
		
		JButton btnTranslateTextNow = new JButton("Translate Text Now");
		btnTranslateTextNow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Translate tr=new Translate(cb,pb);
				setVisible(false);
				tr.setVisible(true);
			}
		});
		btnTranslateTextNow.setForeground(Color.BLACK);
		btnTranslateTextNow.setFont(new Font("Myriad Pro", Font.PLAIN, 19));
		btnTranslateTextNow.setBorder(null);
		btnTranslateTextNow.setBackground(new Color(51, 204, 255));
		btnTranslateTextNow.setBounds(0, 400, 227, 37);
		panel_1.add(btnTranslateTextNow);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(437, 172, 404, 217);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setIcon(new ImageIcon(Welcome.class.getResource("/com/JPG-DOC-re.jpg")));
		
		JLabel lblWelcomeToDigidocs = new JLabel("Welcome to DigiDocs");
		lblWelcomeToDigidocs.setFont(new Font("HP Simplified", Font.PLAIN, 55));
		lblWelcomeToDigidocs.setBounds(387, 33, 480, 70);
		panel.add(lblWelcomeToDigidocs);
		
		JLabel lblNewLabel_3 = new JLabel("Convert any Image to Editable Doc.");
		lblNewLabel_3.setFont(new Font("HP Simplified", Font.PLAIN, 40));
		lblNewLabel_3.setBounds(333, 416, 598, 59);
		panel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Securely and Efficiently");
		lblNewLabel_4.setFont(new Font("HP Simplified", Font.PLAIN, 40));
		lblNewLabel_4.setBounds(417, 456, 480, 59);
		panel.add(lblNewLabel_4);
		
		JLabel label_1 = new JLabel(" ");
		label_1.setIcon(new ImageIcon(Welcome.class.getResource("/com/abc re2.jpeg")));
		label_1.setBounds(971, 11, 83, 82);
		panel.add(label_1);
	}
}
