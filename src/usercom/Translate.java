package usercom;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.ProfileBean;

import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.print.DocFlavor.STRING;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.TexturePaint;

import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JComboBox;

public class Translate extends JFrame {

	private JPanel contentPane;
	private JTextPane textPane;
	static public String id;
	static public String nm;
	static public String dep;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Translate frame = new Translate(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Translate(CredentialBean cd,ProfileBean pb) {
		id=cd.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1064, 623);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(51,205,255));
		panel_1.setBounds(0, 0, 534, 612);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(26, 100, 480, 328);
		panel_1.add(editorPane);
		
		String[] choice1= {"en","ta","te","hi"};
		JComboBox comboBox = new JComboBox(choice1);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBox.setBorder(null);
		comboBox.setBackground(Color.WHITE);
		comboBox.setBounds(112, 440, 116, 31);
		panel_1.add(comboBox);
		
		String[] choice2= {"ta","hi","te","en"};
		JComboBox comboBox_1 = new JComboBox(choice2);
		comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBox_1.setBorder(null);
		comboBox_1.setBackground(Color.WHITE);
		comboBox_1.setBounds(358, 440, 116, 31);
		panel_1.add(comboBox_1);
		
		JButton btnNewButton = new JButton("Translate Now");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String str=editorPane.getText();
				String ch1=(String) comboBox.getSelectedItem();
				String ch2=(String) comboBox_1.getSelectedItem();
				if(ch2.equals("ta")) {
					textPane.setFont(new Font("Latha", Font.PLAIN, 15));
				}
				else if(ch2.equals("te")) {
					textPane.setFont(new Font("Gautami", Font.PLAIN, 15));
				}
				else {
					textPane.setFont(new Font("Arial Unicode MS", Font.PLAIN, 15));
				}
				Translator trans=new Translator();
				String str2=null;
					try {
						str2=trans.translate(ch1, ch2, str);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					textPane.setText(str2);
					
			}
		});
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("HP Simplified", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(255, 51, 0));
		btnNewButton.setBounds(139, 515, 270, 48);
		panel_1.add(btnNewButton);
		
		JButton button = new JButton("<");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Welcome we=new Welcome(cb,pb);
				setVisible(false);
				we.setVisible(true);
			}
		});
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button.setBackground(Color.DARK_GRAY);
		button.setBounds(12, 12, 59, 41);
		panel_1.add(button);
		
		JLabel lblInput = new JLabel("Input :");
		lblInput.setFont(new Font("Dialog", Font.BOLD, 15));
		lblInput.setForeground(new Color(0, 0, 0));
		lblInput.setBounds(26, 82, 55, 16);
		panel_1.add(lblInput);
		
		
		
		JLabel lblFrom = new JLabel("From");
		lblFrom.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblFrom.setBounds(46, 439, 66, 31);
		panel_1.add(lblFrom);
		
		JLabel lblTo = new JLabel("To");
		lblTo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTo.setBounds(313, 440, 45, 31);
		panel_1.add(lblTo);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.DARK_GRAY);
		panel_2.setBounds(534, 0, 530, 612);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		textPane = new JTextPane();
		textPane.setBounds(46, 49, 445, 530);
		textPane.setEditable(false);
//		textPane.setFont(new Font("Latha", Font.PLAIN, 15));
		panel_2.add(textPane);
		
		JLabel lblOutput = new JLabel("Output :");
		lblOutput.setForeground(Color.WHITE);
		lblOutput.setFont(new Font("Dialog", Font.BOLD, 15));
		lblOutput.setBounds(46, 30, 82, 16);
		panel_2.add(lblOutput);
	}
}
