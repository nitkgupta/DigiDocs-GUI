package usercom;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.asprise.imaging.scan.ScanManager;

import bean.CredentialBean;
import bean.ProfileBean;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

public class Upload extends JFrame {
	String path;
	private JPanel contentPane;
	static public String id;
	static public String cat;
	static public String name;
	static public String nm;
	static public String dep;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Upload frame = new Upload(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("Image Upload");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Upload(CredentialBean cb,ProfileBean pb) {
		id = cb.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 511, 611);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel label1 = new JLabel(" ");
		label1.setBounds(0, 0, 511, 611);
		panel.add(label1);

		JButton btnNewButton = new JButton("Select from local");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser filechooser = new JFileChooser();
				filechooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter ef = new FileNameExtensionFilter("Images", "jpg", "png", "jpeg");
				filechooser.addChoosableFileFilter(ef);
				int result = filechooser.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File file = filechooser.getSelectedFile();
					path = file.getAbsolutePath();
					label1.setIcon(ResizeImage(path));
				} else if (result == JFileChooser.CANCEL_OPTION) {
					System.out.println("No File Select");
				}
			}

			private ImageIcon ResizeImage(String ImagePath) {
				ImageIcon MyImage = new ImageIcon(ImagePath);
				Image img = MyImage.getImage();
				Image newImg = img.getScaledInstance(label1.getWidth(), label1.getHeight(), Image.SCALE_SMOOTH);

				ImageIcon image = new ImageIcon(newImg);
				return image;
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBackground(new Color(51, 255, 204));
		btnNewButton.setBounds(842, 37, 162, 43);
		btnNewButton.setBorder(null);
		contentPane.add(btnNewButton);

		JButton btnScan = new JButton("Scan");

		btnScan.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				try {	
				File[] files = ScanManager.getDefaultManager()
						.quickScanUsingUI(new File("C:\\Users\\nitka\\Documents\\My Scans\\abc123.jpg"), null);
				for (File f1 : files) {
					path = f1.getAbsolutePath();
					System.out.println(path);
					label1.setIcon(ResizeImage(path));
				}
				}
				catch (Exception e) {
					JOptionPane pane289 = new JOptionPane();
					pane289.showMessageDialog(null, "Scanner Not Found! Please Try again.");
					
				}

			}

			private Icon ResizeImage(String absolutePath) {
				ImageIcon MyImage = new ImageIcon(absolutePath);
				Image img = MyImage.getImage();
				Image newImg = img.getScaledInstance(label1.getWidth(), label1.getHeight(), Image.SCALE_SMOOTH);

				ImageIcon image = new ImageIcon(newImg);
				return image;

			}

		});
		btnScan.setBackground(new Color(51, 255, 204));
		btnScan.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnScan.setBounds(842, 109, 162, 43);
		btnScan.setBorder(null);
		contentPane.add(btnScan);
		JProgressBar progressBar = new JProgressBar(0,4);
		progressBar.setBackground(Color.WHITE);
		progressBar.setForeground(Color.DARK_GRAY);
		progressBar.setBounds(842, 348, 159, 25);
		progressBar.setValue(0);
		progressBar.setBorder(null);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		contentPane.add(progressBar);
		
		JLabel lblLoading = new JLabel("Loading . . . .");
		lblLoading.setFont(new Font("HP Simplified", Font.PLAIN, 14));
		lblLoading.setForeground(Color.DARK_GRAY);
		lblLoading.setBounds(852, 374, 119, 25);
		lblLoading.setVisible(false);
		contentPane.add(lblLoading);
		
		JButton btnConvertNow = new JButton("Convert Now");
		btnConvertNow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				progressBar.setValue(2);
				progressBar.setVisible(true);
				lblLoading.setVisible(true);				
				JOptionPane pane1 = new JOptionPane();
//				name = pane1.showInputDialog("Enter filename");
//				String[] choice12 = { "Domicile", "Birth Certificate", "Death Certificate", "Driving Licence",
//						"10th Certificate", "12th Certificate", "Graduation Certificate", "Post-Graduation Certificate", "Aadhar Card",
//						"PAN Card", "Voter ID Card" };
//				cat = (String) pane1.showInputDialog(null, "Select Category", "Category", JOptionPane.QUESTION_MESSAGE,
//						null, choice12, choice12[0]);
				String[] choice12 = { "Domicile", "Birth Certificate", "Death Certificate", "Driving Licence",
						"10th Certificate", "12th Certificate", "Graduation Certificate", "Post-Graduation Certificate", "Aadhar Card",
						"PAN Card", "Voter ID" };
				JComboBox comboBox = new JComboBox(choice12);
				comboBox.setBounds(72, 38, 138, 71);
				JTextField name2 = new JTextField();
				JCheckBox js = new JCheckBox();
				String[] choice123 = { "tam","tel","hin","eng" };
				JComboBox comboBox2 = new JComboBox(choice123);
				comboBox2.setBounds(72, 38, 138, 71);
				Object[] message = {
					"Name:",name2,
				    "Category:", comboBox,
				    "Select Language",comboBox2,
				    "English Enabled", js
				};
				String lang;
				String sel;
				
				int option = JOptionPane.showConfirmDialog(null, message, "Details", JOptionPane.OK_CANCEL_OPTION);
		
				name=name2.getText();
						cat=(String)comboBox.getSelectedItem();
						lang=(String)comboBox2.getSelectedItem();
						sel=String.valueOf(js.isSelected());
				HttpUrlTrial2 htp = new HttpUrlTrial2();
				int i = 0;
				
				String path2 = "F:\\DigiDocs-Docs\\" + name + ".doc";
				try {
					progressBar.setValue(3);
					i = htp.sendPOST(path, path2,lang,sel);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (i == 200) {
					System.out.println(i);
					
					CredentialBean cb = new CredentialBean();
					cb.setUserId(id);
					Converted co = new Converted(cb,pb, path, path2,name,cat);
					setVisible(false);
					co.setVisible(true);
				} else {
					progressBar.setValue(4);
					JOptionPane pane2 = new JOptionPane();
					pane2.showMessageDialog(null, "Error in Converting! Please try again.");
				}

			}
		});

		btnConvertNow.setBackground(new Color(51, 255, 204));
		btnConvertNow.setBorder(null);
		btnConvertNow.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConvertNow.setBounds(842, 262, 162, 43);
		contentPane.add(btnConvertNow);

		JButton btnEditImage = new JButton("Edit Image");
		btnEditImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Runtime runTime = Runtime.getRuntime();
					Process process = runTime.exec("mspaint" + " " + path);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnEditImage.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnEditImage.setBorder(null);
		btnEditImage.setBackground(new Color(51, 255, 204));
		btnEditImage.setBounds(842, 187, 162, 43);
		contentPane.add(btnEditImage);

		JButton btnReturn = new JButton("Return");
		btnReturn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb = new CredentialBean();
				cb.setUserId(id);
				Welcome we = new Welcome(cb,pb);

				setVisible(false);
				we.setVisible(true);

			}
		});
		btnReturn.setForeground(Color.WHITE);
		btnReturn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReturn.setBorder(null);
		btnReturn.setBackground(Color.DARK_GRAY);
		btnReturn.setBounds(842, 450, 162, 43);
		contentPane.add(btnReturn);

		JLabel lblpleaseWaitFor = new JLabel("*Please wait after clicking");
		lblpleaseWaitFor.setForeground(Color.RED);
		lblpleaseWaitFor.setBounds(849, 306, 155, 16);
		contentPane.add(lblpleaseWaitFor);
		
		
		
		

	}
}
