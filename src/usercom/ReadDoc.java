package usercom;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.ProfileBean;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ReadDoc extends JFrame {

	private JPanel contentPane;
	static String id;
	static String pa;
	static String pa2;
	static String path;
	static String path2;
	static String ca;
	static String cat;
	static String na;
	static String name;
	static JTextPane textArea;
	static public String nm;
	static public String dep;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReadDoc frame = new ReadDoc(new CredentialBean(),new ProfileBean(),pa,pa2,na,ca);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReadDoc(CredentialBean cb,ProfileBean pb,String pa3,String pa4,String na,String ca) {
		path=pa3;
		path2=pa4;
		name=na;
		cat=ca;
		id=cb.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 1064, 621);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton button = new JButton("<");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb1=new CredentialBean();
				cb1.setUserId(id);
				Converted cv=new Converted(cb,pb,path,path2,name,cat);
				setVisible(false);
				cv.setVisible(true);
			}
		});
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button.setBackground(Color.DARK_GRAY);
		button.setBounds(12, 12, 59, 41);
		panel.add(button);
		
		Readings rd=new Readings();
//		System.out.println(rd.read());
		textArea = new JTextPane();
		textArea.setBounds(114, 22, 852, 569);
		textArea.setText(rd.read(path2));
		panel.add(textArea);
		
//		JScrollPane scrollBar = new JScrollPane(textArea);
//		scrollBar.setBounds(949, 63, 17, 501);
//		scrollBar.add(textArea);
//		panel.add(scrollBar);

}
	}
