package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

public class SaveToPDF {

	/**
     * @param args
     */
    public String save(String path2) {
        // TODO Auto-generated method stub

    	String status="";
         try {  
        	 FileInputStream fds=new FileInputStream(path2);
 			int content;
 			String str="";
 			while((content=fds.read())!=-1) {
 				str=str.concat(Character.toString((char)content));				
 			}
 			//System.out.println(str);
 			str=str.replaceAll(System.lineSeparator()+System.lineSeparator(),System.lineSeparator());
 			String stt=path2.replaceAll(".doc", ".pdf");
 			PdfWriter writer=new PdfWriter(stt);
 			 PdfDocument pdfDoc = new PdfDocument(writer);  
 			 pdfDoc.addNewPage();
 			 Document document = new Document(pdfDoc);
 			 Paragraph para=new Paragraph(str);
 			 document.add(para);
 			 
 			 document.close();              
// 		      System.out.println("PDF Created");   
 			 status="done";
 			 return status;
 		} catch (FileNotFoundException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         return status;
         }
}