package com;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.DocumentBean;
import bean.ProfileBean;
import services.DocumentValid;

import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JTextField;

public class Converted extends JFrame {

	private JPanel contentPane;
	static String id;
	static String pa;
	static String pa2;
	static String path;
	static String path2;
	static String ca;
	static String cat;
	static String na;
	static String name;
	static public String nm;
	static public String dep;
	JOptionPane pane123=new JOptionPane();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Converted frame = new Converted(new CredentialBean(),new ProfileBean(), pa, pa2,na,ca);
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Converted(CredentialBean cb,ProfileBean pb,String pa3,String pa4,String nam,String ca1) {
		path=pa3;
		path2=pa4;
		cat=ca1;
		name=nam;
		id=cb.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(51, 204, 255));
		panel.setBounds(0, 0, 1064, 109);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblDocumentSuccessfullyConverted = new JLabel("Document Successfully Converted...");
		lblDocumentSuccessfullyConverted.setFont(new Font("HP Simplified", Font.PLAIN, 30));
		lblDocumentSuccessfullyConverted.setBounds(60, 31, 510, 52);
		panel.add(lblDocumentSuccessfullyConverted);
		
		JButton btnview = new JButton("Modify/Edit Document");
		btnview.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Desktop desk=Desktop.getDesktop();
				File f=new File(path2);
				try {
					desk.open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnview.setForeground(Color.BLACK);
		btnview.setFont(new Font("Arial", Font.PLAIN, 21));
		btnview.setBorder(null);
		btnview.setBackground(new Color(255, 153, 102));
		btnview.setBounds(194, 251, 227, 49);
		contentPane.add(btnview);
		
		JButton btnCreatePdf = new JButton("Create PDF");
	
		btnCreatePdf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				SaveToPDF sv=new SaveToPDF();
				String sot=sv.save(path2);
				if(sot.equals("done")) {
					JOptionPane pane45=new JOptionPane();
					pane45.showMessageDialog(null,"Pdf has been Successfully generated! Check your Home Directory");
				}
			}
		});
		btnCreatePdf.setForeground(Color.BLACK);
		btnCreatePdf.setFont(new Font("Arial", Font.PLAIN, 21));
		btnCreatePdf.setBorder(null);
		btnCreatePdf.setBackground(new Color(255, 153, 102));
		btnCreatePdf.setBounds(194, 324, 227, 49);
		contentPane.add(btnCreatePdf);
		
		JButton btnRescan = new JButton("Re-Scan");
		btnRescan.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Upload up=new Upload(cb,pb);
				setVisible(false);
				up.setVisible(true);
			}
		});
	
		btnRescan.setForeground(Color.BLACK);
		btnRescan.setFont(new Font("Arial", Font.PLAIN, 21));
		btnRescan.setBorder(null);
		btnRescan.setBackground(new Color(255, 153, 102));
		btnRescan.setBounds(194, 470, 227, 49);
		contentPane.add(btnRescan);
		
		JButton btnReadDocument = new JButton("Read Document");
		btnReadDocument.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				ReadDoc rd=new ReadDoc(cb,pb,path,path2,name,cat);
				setVisible(false);
				rd.setVisible(true);
			}
		});
		btnReadDocument.setForeground(Color.BLACK);
		btnReadDocument.setFont(new Font("Arial", Font.PLAIN, 21));
		btnReadDocument.setBorder(null);
		btnReadDocument.setBackground(new Color(255, 153, 102));
		btnReadDocument.setBounds(194, 178, 227, 49);
		contentPane.add(btnReadDocument);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Upload we=new Upload(cb,pb);
				JOptionPane pn=new JOptionPane();
				int i=pn.showConfirmDialog(new Welcome(cb,pb), "Are you sure ?");
				if(i==0) {
				setVisible(false);
				we.setVisible(true);
				}
			}
		});
		btnCancel.setForeground(Color.WHITE);
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCancel.setBorder(null);
		btnCancel.setBackground(Color.DARK_GRAY);
		btnCancel.setBounds(666, 319, 162, 43);
		contentPane.add(btnCancel);
		
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
//				Complete from here ....add create document etc
				DocumentBean db=new DocumentBean();
				db.setDocumentName(name);
				db.setCreatedByUser(id);
				db.setCategory(cat);
				db.setDepartment("ab106");
				boolean b1=false,b2=false;
				DocumentValid dv=new DocumentValid();
				String che=dv.createDocument(db);
				if(che.equals("INVALID"))
				{
					System.out.println("Document Creation Failed");
				}
				else
				{
					System.out.println(che);
					try {
						b1=dv.uploadDocumentImage(che, path);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(b1);
					if(b1) {
						try {
							b2=dv.saveDocumentOutput(che, path2);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println(b2);
						if(b2) {
							pane123.showMessageDialog(null, "Your Document has been Processed and Saved with ID = "+che);
						}
						else {
							pane123.showMessageDialog(null, "Uploading Failed!! Try Again");
						}
					}
				}
				
			}
		});
		
		btnSave.setForeground(new Color(0, 0, 0));
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSave.setBorder(null);
		btnSave.setBackground(new Color(51, 204, 255));
		btnSave.setBounds(666, 237, 162, 43);
		contentPane.add(btnSave);
		
		JButton btnTranslate = new JButton("Translate");
		btnTranslate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Done Baby");
			}
		});
		btnTranslate.setForeground(Color.BLACK);
		btnTranslate.setFont(new Font("Arial", Font.PLAIN, 21));
		btnTranslate.setBorder(null);
		btnTranslate.setBackground(new Color(255, 153, 102));
		btnTranslate.setBounds(194, 397, 227, 49);
		contentPane.add(btnTranslate);
	}
}
