package com;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Base64;

public class Base64trial {

	public static String encodeFileToBase64Binary(File file) {
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.getEncoder().encode(bytes),"UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return encodedfile;
	}
	public static void decode(String sot,String path2)
	{
		FileWriter fw;
		try {
			byte []b = sot.getBytes("UTF-8");
			Base64.Decoder decode = Base64.getMimeDecoder();
			b = decode.decode(b);
			String s2 = new String(b,"UTF-16");
			fw = new FileWriter(path2);
			fw.write(s2);
			fw.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
