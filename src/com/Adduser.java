package com;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.ProfileBean;
import services.CredentialValid;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;

import javax.swing.JSeparator;
import java.awt.SystemColor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Adduser extends JFrame {
 
	private JPanel contentPane;
	private JTextField fname;
	private JTextField email;
	private JPasswordField passwordField1;
	private JPasswordField passwordField2;
	private JTextField lname;
	private JOptionPane pane;
	static public String id;
	static public String nm;
	static public String dep;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Adduser frame = new Adduser(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("User Creation");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Adduser(CredentialBean bean,ProfileBean pb) {
		id=bean.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080,650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 1064, 623);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(51, 204, 255));
		panel_1.setBounds(323, 79, 428, 447);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel Label1 = new JLabel("First Name");
		Label1.setFont(new Font("Tahoma", Font.BOLD, 16));
		Label1.setBackground(new Color(255, 255, 255));
		Label1.setBounds(30, 58, 112, 35);
		panel_1.add(Label1);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPassword.setBackground(new Color(255, 255, 255));
		lblPassword.setBounds(30, 199, 112, 35);
		panel_1.add(lblPassword);
		
		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setBackground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(30, 152, 112, 35);
		panel_1.add(lblNewLabel);
		
		JLabel lblReenterPassword = new JLabel("Confirm");
		lblReenterPassword.setBackground(new Color(255, 255, 255));
		lblReenterPassword.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblReenterPassword.setBounds(30, 237, 112, 29);
		panel_1.add(lblReenterPassword);
		
		JLabel lblPassword_1 = new JLabel("Password");
		lblPassword_1.setBackground(new Color(255, 255, 255));
		lblPassword_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPassword_1.setBounds(30, 256, 112, 29);
		panel_1.add(lblPassword_1);
		
		fname = new JTextField();
		fname.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				fname.setText("");
			}
		});
		fname.setText("Enter First Name here");
		fname.setToolTipText("");
		fname.setForeground(new Color(0, 0, 0));
		fname.setBackground(new Color(51, 204, 255));
		fname.setBorder(null);
		fname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fname.setBounds(190, 50, 195, 32);
		panel_1.add(fname);
		fname.setColumns(10);
		
		email = new JTextField();
		email.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				email.setText("");
			}
		});
		email.setText("Enter Email here");
		email.setForeground(new Color(0, 0, 0));
		email.setBackground(new Color(51, 204, 255));
		email.setFont(new Font("Tahoma", Font.PLAIN, 14));
		email.setColumns(10);
		email.setBorder(null);
		email.setBounds(190, 149, 195, 29);
		panel_1.add(email);
		
		JLabel lblNewLabel_2 = new JLabel("Department");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_2.setBounds(30, 292, 112, 35);
		panel_1.add(lblNewLabel_2);
		
		passwordField1 = new JPasswordField();
		passwordField1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				passwordField1.setText("");
			}
		});
		passwordField1.setForeground(new Color(0, 0, 0));
		passwordField1.setText("********");
		passwordField1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		passwordField1.setBounds(190, 193, 195, 29);
		passwordField1.setBackground(new Color(51, 204, 255));
		passwordField1.setBorder(null);
		panel_1.add(passwordField1);
		
		passwordField2 = new JPasswordField();
		passwordField2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				passwordField2.setText("");
			}
		});
		passwordField2.setText("********");
		passwordField2.setForeground(new Color(0, 0, 0));
		passwordField2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		passwordField2.setBounds(190, 247, 195, 29);
		passwordField2.setBackground(new Color(51, 204, 255));
		passwordField2.setBorder(null);
		panel_1.add(passwordField2);
		
		String[] choices = { "ab106","bi110", "ci109","co101","el108","me107","me118","re111","an112"};
		JComboBox dept = new JComboBox(choices);
		dept.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dept.setBackground(new Color(255, 255, 255));
		dept.setBounds(190, 294, 195, 35);
		panel_1.add(dept);
		
		String[] stat= {"Active","Passive"};
		
		JButton button1 = new JButton("Create User");
		button1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
			}
		});
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
//				System.out.println(fname.getText());
//				System.out.println(email.getText());
//				System.out.println(passwordField1.getPassword());
//				System.out.println(passwordField2.getPassword());
				CredentialBean cd=new CredentialBean();
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
//				cd.setUserId(id);
				ProfileBean pd=new ProfileBean();
				String pass1=new String(passwordField1.getPassword());
				String pass2=new String(passwordField2.getPassword());
				if(pass1.equals(pass2)){
					cd.setPassword(pass1);
					pd.setDepartmentId(dept.getSelectedItem().toString());
					pd.setFirstName(fname.getText());
					pd.setLastName(lname.getText());
					pd.setEmailId(email.getText());
					CredentialValid vd=new CredentialValid();
					String check=vd.registerUser(cd, pd);
					if(check.equals("INVALID")) {
						pane.showMessageDialog(new Adduser(cb,pb),"Invalid! Please check your details Again");
					}
					else {
						pane.showMessageDialog(new Adduser(cb,pb), "Please remember your UserID.Your UserID is - "+check);
					}
				}
				else {
					pane.showMessageDialog(new Adduser(cb,pb), "Password don't Match");
				}
			}
		});
		
		button1.setBackground(Color.DARK_GRAY);
		button1.setForeground(new Color(255, 255, 255));
		button1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button1.setBounds(104, 353, 228, 46);
		button1.setBorder(null);
		panel_1.add(button1);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(0, 0, 0));
		separator_1.setBounds(190, 179, 195, 2);
		panel_1.add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(new Color(0, 0, 0));
		separator_2.setBounds(190, 225, 195, 2);
		panel_1.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBackground(new Color(0, 0, 0));
		separator_3.setBounds(190, 277, 195, 2);
		panel_1.add(separator_3);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.BLACK);
		separator.setBounds(190, 84, 195, 2);
		panel_1.add(separator);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblLastName.setBackground(Color.WHITE);
		lblLastName.setBounds(30, 106, 112, 35);
		panel_1.add(lblLastName);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBackground(Color.BLACK);
		separator_4.setBounds(190, 130, 195, 2);
		panel_1.add(separator_4);
		
		lname = new JTextField();
		lname.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				lname.setText("");
			}
		});
		lname.setToolTipText("");
		lname.setText("Enter Last Name here");
		lname.setForeground(Color.BLACK);
		lname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lname.setColumns(10);
		lname.setBorder(null);
		lname.setBackground(new Color(51, 204, 255));
		lname.setBounds(190, 96, 195, 32);
		panel_1.add(lname);
		
		JButton button2 = new JButton("<");
		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb=new CredentialBean();
				cb.setUserId(id);
				Welcome we=new Welcome(cb,pb);
				
				setVisible(false);
				we.setVisible(true);
			}
		});
		button2.setForeground(new Color(255, 255, 255));
		button2.setBackground(Color.DARK_GRAY);
		button2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button2.setBounds(10, 11, 59, 41);
		panel.add(button2);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.DARK_GRAY);
		panel_2.setBounds(0, 301, 1064, 322);
		panel.add(panel_2);
		
		JLabel label = new JLabel(" ");
		label.setIcon(new ImageIcon(Adduser.class.getResource("/com/abc re2.jpeg")));
		label.setBounds(969, 12, 83, 82);
		panel.add(label);
	}
}
