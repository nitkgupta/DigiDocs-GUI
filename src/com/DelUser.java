package com;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.ProfileBean;
import services.CredentialValid;
import services.ProfileVaild;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class DelUser extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	static String id;
	static public String nm;
	static public String dep;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DelUser frame = new DelUser(new CredentialBean(),new ProfileBean());
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DelUser(CredentialBean cb,ProfileBean pb) {
		id=cb.getUserId();
		nm=pb.getFirstName();
		dep=pb.getDepartmentId();
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 1080, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(381, 154, 288, 294);
		contentPane.add(panel_1);
		panel_1.setBackground(Color.DARK_GRAY);
		panel_1.setLayout(null);

		textField = new JTextField();
		textField.setBounds(39, 102, 211, 37);
		textField.setBorder(null);
		panel_1.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Delete User");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String user = textField.getText();
				JOptionPane pn = new JOptionPane();
				CredentialBean cb1=new CredentialBean();
				cb1.setUserId(id);
//				int i = pn.showConfirmDialog(new DelUser(cb1), "Are you sure ?");
//				if (i == 1) {
					ArrayList<String> ad = new ArrayList<String>();
					ad.add(user);
					ProfileVaild pf=new ProfileVaild();
					int b=pf.deleteProfile(ad);
//					System.out.println(b);
					CredentialValid cv = new CredentialValid();
					int i1 = cv.deleteCredential(ad);
					if (i1 != 0) {
						pn.showMessageDialog(new DelUser(cb1,pb), "User Deleted Successfully");
//					}
						
				}
					else {
						pn.showMessageDialog(new DelUser(cb1,pb), "User Deleted Failed"); }
			}
		});
		btnNewButton.setFont(new Font("HP Simplified", Font.PLAIN, 22));
		btnNewButton.setBorder(null);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(Color.RED);
		btnNewButton.setBounds(53, 187, 178, 50);
		panel_1.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("UserID :");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("HP Simplified", Font.PLAIN, 20));
		lblNewLabel.setBounds(38, 52, 150, 31);
		panel_1.add(lblNewLabel);

		JLabel lblthisWillPermanently = new JLabel("*This will permanently delete the user");
		lblthisWillPermanently.setForeground(Color.ORANGE);
		lblthisWillPermanently.setBounds(39, 142, 222, 16);
		panel_1.add(lblthisWillPermanently);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(51, 204, 255));
		panel.setBounds(0, 293, 1064, 329);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton button = new JButton("<");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CredentialBean cb2=new CredentialBean();
				cb2.setUserId(id);
				Welcome we=new Welcome(cb2,pb);
				setVisible(false);
				we.setVisible(true);
			}
		});
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button.setBackground(Color.DARK_GRAY);
		button.setBounds(12, 12, 59, 41);
		contentPane.add(button);
	}
}
