package com;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import bean.CredentialBean;
import bean.ProfileBean;
import services.CredentialValid;
import services.ProfileVaild;

import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class SignIn extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignIn frame = new SignIn();
					frame.setVisible(true);
					frame.setResizable(false);
					frame.setTitle("Sign In");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SignIn() {
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 800, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setForeground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(0, 0, 339, 474);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Authorised Access");
		lblNewLabel_1.setFont(new Font("HP Simplified", Font.PLAIN, 25));
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(71, 279, 195, 60);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel(" ");
		lblNewLabel.setIcon(new ImageIcon(SignIn.class.getResource("/com/mygov_logo re.jpeg")));
		lblNewLabel.setBounds(0, 0, 339, 236);
		panel.add(lblNewLabel);
		
		JLabel lblOnly = new JLabel("Only");
		lblOnly.setForeground(Color.WHITE);
		lblOnly.setFont(new Font("HP Simplified", Font.PLAIN, 25));
		lblOnly.setBounds(148, 307, 56, 60);
		panel.add(lblOnly);
 
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBounds(338, 0, 446, 474);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblUserid = new JLabel("UserId :");
		lblUserid.setFont(new Font("Myriad Pro", Font.PLAIN, 17));
		lblUserid.setBounds(104, 79, 131, 30);
		panel_1.add(lblUserid);

		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setBounds(104, 120, 235, 30);
		panel_1.add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Password :");
		lblNewLabel_2.setFont(new Font("Myriad Pro", Font.PLAIN, 17));
		lblNewLabel_2.setBounds(104, 182, 131, 30);
		panel_1.add(lblNewLabel_2);

		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField.setBounds(104, 223, 235, 30);
		panel_1.add(passwordField);

		JButton btnNewButton = new JButton("Sign In");
		btnNewButton.setBorder(null);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String id = textField.getText();
				String pass = new String(passwordField.getPassword());
				CredentialValid cv = new CredentialValid();
				CredentialBean check = cv.viewCredential(id);
				String pass2 = check.getPassword();
				String user= check.getUserType();
				ProfileBean bean;
				ProfileVaild vd=new ProfileVaild();
//				System.out.println(pass);
//				System.out.println(check.getPassword());
//				System.out.println(passwordField.getPassword());
//				System.out.println((check.getPassword()).equals(pass));
				if (pass.equals(pass2) && user.equals("a")) {
					CredentialBean cb=new CredentialBean();
					bean=vd.getNameDept(id);
					
					cb.setUserId(id);
					Welcome we=new Welcome(cb,bean);
					setVisible(false);
					we.setVisible(true);
				}
				else if(pass.equals(pass2) && user.equals("u")) {
					CredentialBean cb=new CredentialBean();
					bean=vd.getNameDept(id);
					cb.setUserId(id);
					usercom.Welcome we=new usercom.Welcome(cb,bean);
					setVisible(false);
					we.setVisible(true);
				}
				else {
					JOptionPane pane=new JOptionPane();
					pane.showMessageDialog(new SignIn(), "Incorrect UserId/Password");
				}
			}
		});
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(204, 51, 51));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(124, 306, 204, 37);
		panel_1.add(btnNewButton);
		
		JLabel label = new JLabel("New label");
		label.setIcon(new ImageIcon(SignIn.class.getResource("/com/abc re2.jpeg")));
		label.setBounds(351, 12, 83, 82);
		panel_1.add(label);

	}
}
