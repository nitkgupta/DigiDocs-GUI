package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUrlTrial2 {
	static String str;
//	public static void main(String[] args) {
//		try {
//			sendPOST();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	public int sendPOST(String path,String path2,String lang,String sel) throws IOException {
		URL obj = new URL("http://b78d8eff.ngrok.io/");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		con.setRequestMethod("POST");
		
		JsonT js=new JsonT();
		String str=js.abc(path,lang,sel);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(str.getBytes("UTF-8"));
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			String response = new String();

			while ((inputLine = in.readLine()) != null) {
				response=response.concat(inputLine);
			}
			in.close();
			Base64trial bs=new Base64trial();
			bs.decode(response,path2);
			return responseCode;
		} else {
			System.out.println("POST request not worked");
			return responseCode;
		}
	}

}
