package com;
import java.io.*;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class Readings
{
    public String read(String path2)
    {
        File file = null;
        String str = "" ;
        try
        {
        	 FileInputStream fds=new FileInputStream(path2);
  			int content;
  			while((content=fds.read())!=-1) {
  				str=str.concat(Character.toString((char)content));				
  			}
  			str=str.replaceAll(System.lineSeparator()+System.lineSeparator(),System.lineSeparator());

            return str;
        }
        catch (Exception exep)
        {
            exep.printStackTrace();
        }
        return str;
    }
}